# sw4j.org build configuration

This is the build configuration data for sw4j.org projects.

This configuration contains files for
* findbugs
* checkstyle
* pmd
* OWASP dependecy check
* versions plugin
